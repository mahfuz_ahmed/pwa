# Progressive Web Demo App

## How to run this project
1. copy all files under the public folder to htdocs
2. change vapid keys in /firebase/functions/index.js and /public/src/sw.js 
3. deploy index.js from /firebase/functions/ to firebase cloud functions

Step 2 and 3 are optional since it's already deployed and will stay there for a while.
